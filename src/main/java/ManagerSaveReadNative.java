import java.io.*;

public class ManagerSaveReadNative implements Iremote <User>{
    private final String EXTENSION = "xxx";

    @Override
    public void save(String fileName, User user) throws IOException {
        FileOutputStream fileOutputStream= new FileOutputStream(fileName+"."+EXTENSION);
        ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutputStream);
        objectOutput.writeObject(user);
        objectOutput.close();
    }

    @Override
    public User load(String fileName) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(fileName+"."+EXTENSION);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        User newUser = (User) objectInputStream.readObject();
        objectInputStream.close();
        return newUser;
    }
}
