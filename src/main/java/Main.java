import java.io.IOException;

public class Main {

    private ServiceLocator serviceLocator = new ServiceLocator();


    public void start() {
        User user = new User();
        user.lifeLevel = 55;
        user.health = 22;
        Sord sord = new Sord();
        sord.level = 5;
        user.sord = sord;


        Iremote<User> managerSaveRead = serviceLocator.getManager();

        try {
            managerSaveRead.save("File1",user);
            User newUser = managerSaveRead.load("File1");

            System.out.println("lifeLevel " + newUser.lifeLevel);
            System.out.println("health " + newUser.health);
            System.out.println("sord " + newUser.sord.level);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws Exception{
       Main main = new Main();
       main.start();
    }
}
