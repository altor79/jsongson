import java.io.IOException;

public interface Iremote <T> {
    public void save(String fileName, T object) throws IOException;
    T load(String fileName) throws IOException, ClassNotFoundException;
}
